import UploadComponent from './src/component.vue'

export default {
    install (Vue, config) {
        const Upload = Vue.extend(UploadComponent)

        Vue.prototype.$upload = {
            open (options = {}) {
                if (!Upload) {
                    return false
                }
                let show = false
                const handle = new Upload({
                    el: document.createElement('div')
                })
                handle.module = options.module
                handle.multiple = options.multiple
                handle.callback = options.callback
                if (config.uploadImage) {
                    handle.request = config.uploadImage
                }

                document.body.appendChild(handle.$el)

                handle.show = true

                return handle
            }
        }
    }
}
