const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const CleanWebpackPlugin = require('clean-webpack-plugin')


module.exports = {
    mode: 'production',
    entry: {
        index: './src/index.js',
        'editor/index': './src/editor/index.js',
        'upload/index': './src/upload/index.js',
    },
    output: {
        path: path.join(__dirname, 'lib'),
        filename: '[name].js',
        libraryTarget: 'umd'
        // library: 'KeEditor'
    },
    devtool: "source-map",
    externals: {
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                loader: 'vue-loader'
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        // new CleanWebpackPlugin({ path: './dist' })
    ]
}
