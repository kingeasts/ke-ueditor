1. 因为ueditor依赖jquery，所以组件按需加载了jquery
2. ueditor需要放在public/static下面，这样打包不会增加入口文件的大小,只有用到ueditor才会加载


## 安装

```
npm i ke-ueditor
or
yarn add ke-ueditor
```


## 使用，以@vue/cli3为标准
1. 去官网下载ueditor   https://ueditor.baidu.com/website/download.html
2. 解压到public/static

```
<template>
    <div>
        <u-editor v-model="content" :ueditor="`${process.env.BASE_URL}static/ueditor/`"></u-editor>
        <!--
            需要注意的是v-model与ueditor必须传入
            v-model  绑定为数据
            ueditor 为ueditor的目录,只需要目录,不需要文件
            jquery 可留空,留空时使用bootcdn的jquery，传入false则不会加载jquery,需要自己全局加载
            upload-image 图片上传方法
            options 初始化选项,具体请查看ueditor官方文档
            toolbar 工具栏图标,具体请查看ueditor官方文档
        -->
    </div>
</template>
<script>
import UEditor from 'ke-ueditor/lib/editor'
export default {
    components: { UEditor },
    data () {
        return {
            content: 'Hello Ueditor'
        }
    }
}
</script>
```

觉得好用的点个赞喔 :bowtie: 
